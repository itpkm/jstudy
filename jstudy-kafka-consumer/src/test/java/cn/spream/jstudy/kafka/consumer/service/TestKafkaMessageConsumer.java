package cn.spream.jstudy.kafka.consumer.service;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-9-8
 * Time: 下午2:24
 * To change this template use File | Settings | File Templates.
 */
public class TestKafkaMessageConsumer {

    private KafkaMessageConsumer kafkaMessageConsumer;

    {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-consumer.xml");
        kafkaMessageConsumer = (KafkaMessageConsumer) context.getBean("kafkaMessageConsumer");
    }

    @Test
    public void testReceive() {
        kafkaMessageConsumer.receive(new KafkaMessageConsumer.Callback() {
            @Override
            public void onMessage(String message) {
                System.out.println("message:" + message);
            }
        });
    }

}
