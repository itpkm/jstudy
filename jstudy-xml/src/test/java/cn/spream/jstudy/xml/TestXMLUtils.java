package cn.spream.jstudy.xml;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.Node;
import org.junit.Test;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-3-26
 * Time: 上午8:53
 * To change this template use File | Settings | File Templates.
 */
public class TestXMLUtils {

    private static Log log = LogFactory.getLog(TestXMLUtils.class);

    @Test
    public void test() {
        String xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<result>\n" +
                "    <head version=\"1\" timestamp=\"13436841534315\" error=\"0\"/>\n" +
                "    <body>\n" +
                "        <users>\n" +
                "            <user>\n" +
                "                <id>100</id>\n" +
                "                <name>张三</name>\n" +
                "                <sex>1</sex>\n" +
                "                <age>22</age>\n" +
                "            </user>\n" +
                "            <user>\n" +
                "                <id>101</id>\n" +
                "                <name>李四</name>\n" +
                "				 <tels>" +
                "					<tel>1234</tel>" +
                "					<tel>4567</tel>" +
                "					<tel>6789</tel>" +
                "				 </tels>" +
                "                <sex>0</sex>\n" +
                "                <age>23</age>\n" +
                "            </user>\n" +
                "        </users>\n" +
                "    </body>\n" +
                "</result>";
        try {
            Document document = XMLUtils.parseXML(xml);
            String version = XMLUtils.getNodeText(document, "/result/head/@version");
            String timestamp = XMLUtils.getNodeText(document, "/result/head/@timestamp");
            String error = XMLUtils.getNodeText(document, "/result/head/@error");
            System.out.println("head version:" + version);
            System.out.println("head timestamp:" + timestamp);
            System.out.println("head error:" + error);
            System.out.println("--------------------");
            List<Node> users = XMLUtils.getListNode(document, "/result/body/users/user");
            for (Node user : users) {
                String id = XMLUtils.getNodeText(user, "./id");
                String name = XMLUtils.getNodeText(user, "./name");
                List<Node> tels = XMLUtils.getListNode(user, "./tels/tel");
                String sex = XMLUtils.getNodeText(user, "./sex");
                String age = XMLUtils.getNodeText(user, "./age");
                System.out.println("id:" + id);
                System.out.println("name:" + name);
                for (Node tel : tels) {
                    System.out.println(tel.getText());
                }
                System.out.println("sex:" + sex);
                System.out.println("age:" + age);
                System.out.println("--------------------");
            }
        } catch (Exception e) {
            log.error("解析xml时出错了", e);
        }
    }

}
