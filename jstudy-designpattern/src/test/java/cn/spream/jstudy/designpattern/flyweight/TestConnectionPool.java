package cn.spream.jstudy.designpattern.flyweight;

import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-15
 * Time: 下午3:05
 * To change this template use File | Settings | File Templates.
 */
public class TestConnectionPool {

    @Test
    public void test() throws SQLException, ClassNotFoundException {
        ConnectionPool connectionPool = new ConnectionPool();
        Connection connection1 = connectionPool.getConnection();
        System.out.println("connectionPool.size=" + connectionPool.size());
        Connection connection2 = connectionPool.getConnection();
        System.out.println("connectionPool.size=" + connectionPool.size());
        Connection connection3 = connectionPool.getConnection();
        System.out.println("connectionPool.size=" + connectionPool.size());
        Connection connection4 = connectionPool.getConnection();
        System.out.println("connectionPool.size=" + connectionPool.size());
        connectionPool.release(connection1);
        System.out.println("connectionPool.size=" + connectionPool.size());
        connectionPool.release(connection2);
        System.out.println("connectionPool.size=" + connectionPool.size());
        connectionPool.release(connection3);
        System.out.println("connectionPool.size=" + connectionPool.size());
        connectionPool.release(connection4);
        System.out.println("connectionPool.size=" + connectionPool.size());
    }

}
