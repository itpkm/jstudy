package cn.spream.jstudy.designpattern.abstractfactory;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-6
 * Time: 下午2:37
 * To change this template use File | Settings | File Templates.
 */
public class TestSenderFactory {

    @Test
    public void send() {
        String info = "Hello Abstract Factory";
        SenderFactory senderFactory = new MailSenderFactory();
//        SenderFactory senderFactory = new SmsSenderFactory();
        senderFactory.produce().send(info);

    }

}
