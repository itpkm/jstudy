package cn.spream.jstudy.designpattern.mediator;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15/1/18
 * Time: 下午3:26
 * To change this template use File | Settings | File Templates.
 */
public abstract class Mediator {

    protected Colleague colleagueA;
    protected Colleague colleagueB;

    public Mediator(Colleague colleagueA, Colleague colleagueB) {
        this.colleagueA = colleagueA;
        this.colleagueB = colleagueB;
    }

    public abstract void affectA();

    public abstract void affectB();

}
