package cn.spream.jstudy.designpattern.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-15
 * Time: 下午4:43
 * To change this template use File | Settings | File Templates.
 */
public class ConcreteWatched implements Watched {

    private List<Watcher> watchers = new ArrayList<Watcher>();

    @Override
    public void addWatcher(Watcher watcher) {
        watchers.add(watcher);
    }

    @Override
    public void removeWatcher(Watcher watcher) {
        watchers.remove(watcher);
    }

    @Override
    public void notifyWatchers(String str) {
        for (Watcher watcher : watchers){
            watcher.update(str);
        }
    }

}
