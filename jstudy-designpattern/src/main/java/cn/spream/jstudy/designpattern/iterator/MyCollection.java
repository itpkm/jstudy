package cn.spream.jstudy.designpattern.iterator;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-16
 * Time: 上午10:54
 * To change this template use File | Settings | File Templates.
 */
public class MyCollection implements Collection {

    private Object[] elements = new Object[10];
    private int size;

    @Override
    public Iterator iterator() {
        return new MyIterator(this);
    }

    public boolean add(Object o) {
        int len = elements.length;
        if(size + 1 > len){
            elements = Arrays.copyOf(elements, elements.length + 10);
        }
        elements[size++] = o;
        return true;
    }

    @Override
    public Object get(int i) {
        return elements[i];
    }

    @Override
    public int size() {
        return size;
    }

}
