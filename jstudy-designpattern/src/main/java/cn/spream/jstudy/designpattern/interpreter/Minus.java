package cn.spream.jstudy.designpattern.interpreter;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15/1/18
 * Time: 下午3:55
 * To change this template use File | Settings | File Templates.
 */
public class Minus implements Expression {

    @Override
    public int interpret(Context context) {
        return context.getNum1() - context.getNum2();
    }

}
