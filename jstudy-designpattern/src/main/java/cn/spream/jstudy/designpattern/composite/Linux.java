package cn.spream.jstudy.designpattern.composite;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 下午4:35
 * To change this template use File | Settings | File Templates.
 */
public class Linux {

    private TreeNode root;

    public Linux() {
        root = new TreeNode("linux");
        TreeNode home = new TreeNode("home");
        TreeNode mysql = new TreeNode("mysql");
        mysql.setParent(home);
        home.addChild(mysql);
        TreeNode apache = new TreeNode("apache");
        apache.setParent(home);
        home.addChild(apache);
        home.setParent(root);
        root.addChild(home);
        TreeNode etc = new TreeNode("etc");
        etc.setParent(root);
        root.addChild(etc);
        TreeNode tmp = new TreeNode("tmp");
        tmp.setParent(root);
        root.addChild(tmp);
    }

    public TreeNode getRoot() {
        return root;
    }
}
