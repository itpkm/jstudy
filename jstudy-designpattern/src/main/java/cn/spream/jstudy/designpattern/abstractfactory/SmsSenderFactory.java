package cn.spream.jstudy.designpattern.abstractfactory;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-6
 * Time: 下午2:26
 * To change this template use File | Settings | File Templates.
 */
public class SmsSenderFactory implements SenderFactory {

    @Override
    public Sender produce() {
        return new SmsSender();
    }
}
