package cn.spream.jstudy.designpattern.adapter;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 上午10:51
 * To change this template use File | Settings | File Templates.
 */
public class ObjectAdapterLog implements Log {

    private Log4j log4j;

    public ObjectAdapterLog(Log4j log4j) {
        this.log4j = log4j;
    }

    @Override
    public void debug(String debug) {
        System.out.println("ObjectAdapterLog.debug：" + debug);
    }

    @Override
    public void info(String info) {
        log4j.info(info);
    }

    @Override
    public void warn(String warn) {
        System.out.println("ObjectAdapterLog.warn：" + warn);
    }

    @Override
    public void error(String error) {
        log4j.error(error);
    }

    @Override
    public void fatal(String fatal) {
        System.out.println("ObjectAdapterLog.fatal：" + fatal);
    }
}
