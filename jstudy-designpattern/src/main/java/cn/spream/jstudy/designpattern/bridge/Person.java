package cn.spream.jstudy.designpattern.bridge;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 下午3:29
 * To change this template use File | Settings | File Templates.
 */
public abstract class Person {

    private Car car;

    public void drive(){
        car.run();
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }
}
