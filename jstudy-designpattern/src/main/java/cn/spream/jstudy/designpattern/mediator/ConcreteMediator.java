package cn.spream.jstudy.designpattern.mediator;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15/1/18
 * Time: 下午3:31
 * To change this template use File | Settings | File Templates.
 */
public class ConcreteMediator extends Mediator {

    public ConcreteMediator(Colleague colleagueA, Colleague colleagueB) {
        super(colleagueA, colleagueB);
    }

    @Override
    public void affectA() {
        int number = colleagueB.getNumber();
        colleagueA.setNumber(number / 100);
    }

    @Override
    public void affectB() {
        int number = colleagueA.getNumber();
        colleagueB.setNumber(number * 100);
    }

}
