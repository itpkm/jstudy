package cn.spream.jstudy.designpattern.facade;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 下午2:36
 * To change this template use File | Settings | File Templates.
 */
public class OS {

    public void startup(){
        System.out.println("os启动");
    }

    public void shutdown(){
        System.out.println("os停止");
    }

}
