package cn.spream.jstudy.designpattern.templatemethod;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-15
 * Time: 下午3:31
 * To change this template use File | Settings | File Templates.
 */
public interface Calculator {

    public String getOperator();

    public int calculate(String exp);

    public int calculate(int num1, int num2);

}
