package cn.spream.jstudy.activemq.consumer.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-8-14
 * Time: 下午6:01
 * To change this template use File | Settings | File Templates.
 */
public class MqMessageListener implements MessageListener {

    private Log log = LogFactory.getLog(MqMessageListener.class);

    @Override
    public void onMessage(Message message) {
        TextMessage textMessage = (TextMessage) message;
        try {
            log.info("收到消息：" + textMessage.getText());
        } catch (JMSException e) {
            log.error("处理JMS消息出错了：", e);
        }
    }

}
