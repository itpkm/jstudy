package cn.spream.jstudy.thread;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-7-1
 * Time: 下午2:50
 * To change this template use File | Settings | File Templates.
 */
public class TestCountDownLatch {

    @Test
    public void test(){
        try {
            CountDownLatch countDownLatch = new CountDownLatch(5);
            CountDownLatchWorker countDownLatchWorker1 = new CountDownLatchWorker("1", countDownLatch);
            CountDownLatchWorker countDownLatchWorker2 = new CountDownLatchWorker("2", countDownLatch);
            CountDownLatchWorker countDownLatchWorker3 = new CountDownLatchWorker("3", countDownLatch);
            CountDownLatchWorker countDownLatchWorker4 = new CountDownLatchWorker("4", countDownLatch);
            CountDownLatchWorker countDownLatchWorker5 = new CountDownLatchWorker("5", countDownLatch);
            long start = System.currentTimeMillis();
            System.out.println("开始执行：" + start);
            countDownLatchWorker1.start();
            countDownLatchWorker2.start();
            countDownLatchWorker3.start();
            countDownLatchWorker4.start();
            countDownLatchWorker5.start();
            countDownLatch.await();
            long end = System.currentTimeMillis();
            System.out.println("结束执行：" + end);
            System.out.println("执行耗时：" + (end - start) + "毫秒");
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

}
