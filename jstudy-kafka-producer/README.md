1. 下载kafka、scala、zookeeper
2. 配置scala环境变量
3. 配置、启动zookeeper
4. 配置kafka_2.10-0.8.2.1\config\server.properties
5. 启动D:\kafka_2.10-0.8.2.1\bin\windows\kafka-server-start.bat D:\kafka_2.10-0.8.2.1\config\server.properties
6. 错误：Unrecognized VM option '+UseCompressedOops'解决
    找到kafka_2.10-0.8.2.1\bin\windows\kafka-run-class.bat
    找到KAFKA_JVM_PERFORMANCE_OPTS
    去掉-XX:+UseCompressedOops参数
7. 执行TestKafkaMessageProducer.testSend方法发送消息
8. 执行TestKafkaMessageConsumer.testReceive接收消息
