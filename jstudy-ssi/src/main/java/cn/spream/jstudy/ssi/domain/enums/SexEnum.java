package cn.spream.jstudy.ssi.domain.enums;

/**
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-5-16
 * Time: 下午4:32
 * To change this template use File | Settings | File Templates.
 */
public enum SexEnum {

    MAN(0, "男"),
    WOMAN(1, "女");

    private int key;
    private String value;

    private SexEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }
    
    public static SexEnum getEnumByKey(int key){
        for(SexEnum sexEnum : SexEnum.values()){
            if(sexEnum.getKey() == key){
                return sexEnum;
            }
        }
        return null;
    }
    
    public static String getValueByKey(int key){
        SexEnum sexEnum = getEnumByKey(key);
        return sexEnum == null ? null : sexEnum.getValue();
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
